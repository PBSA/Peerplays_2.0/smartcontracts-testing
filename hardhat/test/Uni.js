const { expect } = require("chai");


describe("UNI Token contract", function () {

    async function deployUNIToken() {
        const Token = await ethers.getContractFactory("Uni");
        const [account1, account2] = await ethers.getSigners();

        const accountAddress = account1.address;

        const minterAddress = account2.address;

        const mintingAllowedAfter = Math.floor(Date.now() / 1000) + (365 * 24 * 60 * 60);

        const UniToken = await Token.deploy(accountAddress, minterAddress, mintingAllowedAfter);

        await UniToken.deployed();

        return { Token, UniToken, accountAddress, minterAddress };
    }

    describe("Development", function () {

        it("Should display the token symbol", async function () {
            const { UniToken } = await deployUNIToken();
            const symbol = await UniToken.symbol();
            console.log(symbol);
            expect(symbol).to.equal("UNI");
        });

        it("Should display totalSupply", async function () {
            const { UniToken } = await deployUNIToken();
            const totalSupply = await UniToken.totalSupply();

            expect(totalSupply).to.equal(1000000000000000000000000000n);
        });

        it("Should display the balanceOf accountAddress", async function () {
            const { UniToken, accountAddress, minterAddress } = await deployUNIToken();

            const balanceOfAccount = await UniToken.balanceOf(accountAddress);
            expect(balanceOfAccount).to.equal(1000000000000000000000000000n);
        });

        it("Should transfer tokens from accountAddress to another", async function () {
            const { UniToken, accountAddress, minterAddress } = await deployUNIToken();

            // transfer 50 tokens from accountAddress to minterAddress
            const amount = ethers.BigNumber.from("50");
            await UniToken.transfer(minterAddress, amount);
            const totalSupply = await UniToken.totalSupply();

            // check that accountAddress balance has decreased by amount
            const balanceOfAccount = await UniToken.balanceOf(accountAddress);
            expect(balanceOfAccount).to.equal(totalSupply.sub(amount));

            // check that minterAddress balance has increased by amount
            const balanceOfMinter = await UniToken.balanceOf(minterAddress);
            expect(balanceOfMinter).to.equal(amount);
        });
    });

    describe("mint", function () {
        it("should mint tokens", async function () {
            const { UniToken, minterAddress } = await deployUNIToken();

            // mint 1000 tokens to minter address
            const amount = 1000;
            await UniToken.connect(ethers.provider.getSigner(minterAddress)).mint(minterAddress, amount, { gasLimit: 1000000 });

            // check if the balance of the minter address is updated
            const minterBalance = await UniToken.balanceOf(minterAddress);
            expect(minterBalance).to.equal(0);
        });
    });
});
