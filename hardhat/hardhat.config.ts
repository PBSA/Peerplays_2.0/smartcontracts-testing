import { HardhatUserConfig } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";
require('dotenv').config();


const MNEMONIC_PHRASE: string | undefined = process.env.MNEMONIC_PHRASE;
const MNEMONIC_PEERPLAYS: string | undefined = process.env.MNEMONIC_PEERPLAYS;

if (MNEMONIC_PHRASE === undefined && MNEMONIC_PEERPLAYS === undefined ) {
  console.error("MNEMONIC_PHRASE not found in environment variables");
  process.exit(1);
}

const config: HardhatUserConfig = {
  defaultNetwork: 'peerplays_dev',
  networks: {
     moonbeam_dev: {
       url: 'https://dev.peerplays.network/moonbeam-rpc',
       accounts: [
        "0x8075991ce870b93a8870eca0c0f91913d12f47948ca0fd25b49c6fa7cdbeee8b",
        "0x5fb92d6e98884f76de468fa3f6278f8807c48bebc13595d45af5bdc4da702133",
       ],
       chainId: 1281,
    },
    peerplays_dev: {
      url: 'https://dev.peerplays.network/peerplays-rpc',
       //url: 'http://localhost:9933',
       accounts: [
        "0x398f0c28f98885e046333d4a41c19cee4c37368a9832c6502f6cfd182e2aef89",
        "0xe5be9a5092b81bca64be81d212e7f2f9eba183bb7a90954f7b76361f6edb5c0a",
        "0x0620f2aa019c28d0e725b505e805312f31a576f765b93623695896aa0d92ee88"
       ],
       chainId: 33,
     }
  },
  solidity: {
    compilers: [
      {
        version: "0.5.16",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200
          }
        }
      },
      {
        version: "0.7.6",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200
          }
        }
      },
      {
        version: "0.8.13",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200
          }
        }
      }
    ]
  },
  paths: {
    sources: "./contracts",
    tests: "./test",
    cache: "./cache", 
    artifacts: "./artifacts"
  },
  mocha: {
    timeout: 40000
  }
};

export default config;
