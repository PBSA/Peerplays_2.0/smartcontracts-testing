// const { ethers } = require('hardhat');
const fs = require('fs');

// Deploy function
async function boredApeYachtClubDeploy() {
  const [deployer] = await ethers.getSigners();

  const name = 'My NFT';
  const symbol = 'NFT';
  const maxNftSupply = 1000;
  const saleStart = Math.floor(Date.now() / 1000) + (365 * 24 * 60 * 60);

  console.log(`Deploying ${name} contract using ${deployer.address}`);

  // Deploy the contract
  const Contract = await ethers.getContractFactory('BoredApeYachtClub');
  const contractInstance = await Contract.deploy(name, symbol, maxNftSupply, saleStart);
  await contractInstance.deployed();

  console.log(`${name} contract has been deployed to: ${contractInstance.address}`);

  let deployedDetails = {
    deployerAddress: deployer.address,
    contractAddress: contractInstance.address,
    name: name,
    symbol: symbol,
    maxNftSupply: maxNftSupply,
    saleStart: saleStart,
  }

  fs.writeFileSync('ContractDeployedDetails/BoredApeYachtClub.json', JSON.stringify(deployedDetails, null, 4));
  console.log(`Deployed details are saved in BoredApeYachtClub.json`);
}

boredApeYachtClubDeploy().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
