const { ethers } = require('hardhat');
const fs = require("fs");

// Deploy function
async function uniDeploy() {
  const [account1, account2] = await ethers.getSigners();

  const accountAddress = account1.address;
  console.log(`Deploying UNI contracts using ${accountAddress}`);

  // Define the address for the minter
  const minterAddress = account2.address;

  // Get the current block timestamp
  const mintingAllowedAfter = Math.floor(Date.now() / 1000) + (365 * 24 * 60 * 60);

  // Deploy the contract
  const contract = await ethers.getContractFactory('Uni');
  const contractInstance = await contract.deploy(accountAddress, minterAddress, mintingAllowedAfter);
  await contractInstance.deployed();

  console.log(`Your contract has been deployed to : ${contractInstance.address}`);

  let deployedDetails = {
    deployerAddress: contractInstance.signer.address,
    contractAddress: contractInstance.address,
    accountAddress: account1.address,
    minterAddress: account2.address,
    mintingAllowedAfter: mintingAllowedAfter
  }

  fs.writeFile('ContractDeployedDetails/UNI.json', JSON.stringify(deployedDetails, null, 4), (err: any) => {
    if (err) {
      return console.log(err);
    }
    return console.log("Deployed details are saved in deployedDetails.json")
  })
}

uniDeploy().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});